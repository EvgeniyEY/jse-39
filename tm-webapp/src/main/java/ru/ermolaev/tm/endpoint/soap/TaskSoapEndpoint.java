package ru.ermolaev.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.service.ITaskService;
import ru.ermolaev.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public class TaskSoapEndpoint {

    @Autowired
    private ITaskService taskService;

    @NotNull
    @WebMethod
    public TaskDTO createTask(
            @WebParam(name = "taskDTO", partName = "taskDTO") @Nullable final TaskDTO taskDTO
    ) throws Exception {
        return TaskDTO.toDTO(taskService.createTask(
                taskDTO.getUserId(),
                taskDTO.getName(),
                taskDTO.getProjectId(),
                taskDTO.getDescription()));
    }

    @Nullable
    @WebMethod
    public TaskDTO updateById(
            @WebParam(name = "taskDTO", partName = "taskDTO") @Nullable final TaskDTO taskDTO
    ) throws Exception {
        return TaskDTO.toDTO(taskService.updateById(
                taskDTO.getId(),
                taskDTO.getName(),
                taskDTO.getDescription()));
    }

    @NotNull
    @WebMethod
    public Long countAllTasks() {
        return taskService.countAllTasks();
    }

    @Nullable
    @WebMethod
    public TaskDTO findById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws Exception {
        return TaskDTO.toDTO(taskService.findById(id));
    }

    @NotNull
    @WebMethod
    public List<TaskDTO> findAll() {
        return taskService.findAll();
    }

    @WebMethod
    public void removeOneById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws Exception {
        taskService.removeOneById(id);
    }

}
