package ru.ermolaev.tm.exception.empty;

import ru.ermolaev.tm.exception.AbstractException;

public final class EmptyDateException extends AbstractException {

    public EmptyDateException() {
        super("Error! Date is empty.");
    }

}
