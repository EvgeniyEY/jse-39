package ru.ermolaev.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.listener.AbstractListener;

import java.util.List;

@Component
public class ArgumentsListener extends AbstractListener {

    private List<AbstractListener> commandList;

    @Autowired
    public void setCommandList(final List<AbstractListener> commandList) {
        this.commandList = commandList;
    }

    @NotNull
    @Override
    public String command() {
        return "arguments";
    }

    @NotNull
    @Override
    public String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String description() {
        return "Show application's arguments.";
    }

    @Override
    @EventListener(condition = "(@argumentsListener.command() == #event.name) || (@argumentsListener.arg() == #event.name)")
    public void handler(final ConsoleEvent event) {
        for (@NotNull final AbstractListener command : commandList) {
            if (command.arg() == null) continue;
            System.out.println(command.arg());
        }
    }

}
